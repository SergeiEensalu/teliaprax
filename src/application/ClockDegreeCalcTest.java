package application;

import static org.junit.Assert.*;

import org.junit.Test;

public class ClockDegreeCalcTest {

	@Test
	public void test() {
		ClockDegreeCalc a = new ClockDegreeCalc();
		
		assertEquals(180, a.nurk(18, 0));
		assertEquals(0, a.nurk(12, 0));
		assertEquals(180, a.nurk(6, 0));
	}

}
